<?php 
require_once("backend.interface.php");

class YoutubeParser implements BackendExtractor
{
	public function __construct()
	{
		$this->maxLimit = 5;
	}
	
	private function innerHTML($el) 
	{
 		$doc = new DOMDocument();
  		$doc->appendChild($doc->importNode($el, TRUE));
  		$html = trim($doc->saveHTML());
  		$tag = $el->nodeName;
  		return preg_replace('@^<' . $tag . '[^>]*>|</' . $tag . '>$@', '', $html);
	}
	
	public function getTags($link, $encode=false)
	{
		if ( stripos($link, "Youtube") === false )
			return array();
			
		$site = file_get_contents($link);
		$doc = new DOMDocument();
		$doc->loadHTML($site);
		$body = $doc->getElementById("eow-tags");
		
		$x= explode("\n", $this->innerHTML($body));
		foreach($x as $s)
		{
			$s = strip_tags(html_entity_decode(htmlspecialchars($s)));
			$s = trim($s);
		//	print "String: ".$s.", value: ".strlen($s)."<br />";
			if ( empty($s) || strlen($s) < 4 || is_numeric($s) ) continue;
			$ar2[$s] = 1.0;
		}
		
		$ar2= array_slice($ar2,0,$this->maxLimit);
	//	print_r($ar2);		
		return $ar2;
	}
	
	public function getConceptTags($link)
	{
		return getTags($link, false);
	}
	
	public function setLimitMaxTags($limit)
	{
		$this->maxLimit = $limit;
	}
	
	public function encode($str)
	{
		$temp = split(" ",$str);
		$tempx = join("_", $temp);
		return $tempx;
	}
	
	public function decode($str)
	{
		return str_replace("_", " ", $str);	
	}
	
	private $maxLimit;
}
?>
