<?php 
require_once("OpenCalais.php");
require_once("backend.interface.php");
require_once("backendoperations.php");

//see also http://www.facebook.com/note.php?note_id=389414033919

class OpenCalaisExtractor implements BackendExtractor
{
	
	private $base64;
	private $maxLimit;
	private $relevance_min;
	
	public function __construct()
	{
		$this->calaisObj = new OpenCalais("8m84he4nw8sqpjs58yn2982a");
		$this->base64 = false;
		$this->maxLimit = 5;
		$this->relevance_min = 0.4;
	}
	
	public function setLimitMaxTags($limit)
	{
		$this->maxLimit = $limit;
	}

	public function getLinkData($link)
	{
		$ch = curl_init();
 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11');
 
		$data = curl_exec($ch);
		curl_close($ch);
 
		return $data;
	}
	
	public function getConceptTags($link)
	{
		
	}
	
	public function encode($str)
	{
		
	}
	
	public function decode($str)
	{
		
	}
		
	public function getTags($link, $encode=false)
	{
	  static $max_retry_count = 5;
	  static $retry_count = 0;
	   	
	  try {
			$arr = array();
			
			$content = $this->getLinkData($link);	
			$this->calaisObj->get($content);
			
			//for blogs.discovery.com/discovery-insider/2011/08/new-series-carfelllas.html
			//or such links, gives a warning, Social tags missing?
			$tags = $this->calaisObj->getSocialTags();
			if (!count($tags)) throw new Exception("This link could not be tagged");
			/*
			print "<pre>";
			print "Social tags: <br />";
			print_r($tags);
			print "</pre>";
			*/
			
			foreach($tags as $key=>&$value)
			{ 
				$value = (double)$value/2;
				if ( $value == 1) $value = $this->relevance_min;
				$arr[de_underscore($key)]=$value;
			}
			
			$entities = $this->calaisObj->getEntities();
			
			foreach ($entities as $key=>$value)
			{
				$ar_relevance = 0;
				$count = 0;
				
				foreach($value as $key2=>$value2)
				{
					//$arr[$key][$value2['instances'][0]->exact] = $value2['relevance'];
					$ar_relevance += $value2['relevance'];
					$count++;
				}
				
				$ar_relevance = $ar_relevance / $count;
							
				foreach($value as $key2=>$value2)
				{
					if ( $value2['relevance'] >= $ar_relevance && $value2['relevance'] >= $this->relevance_min )
						$arr[de_underscore($value2['instances'][0]->exact)] = $value2['relevance'];
				}
				
			}
			
			arsort($arr);
			//return only the maxlimit elements from the array
			//foreach($arr as $val)
			$arr = array_slice($arr,0,$this->maxLimit);
			
			$retry_count = 0;
			return $arr;
		}catch(OpenCalaisException $e)
		{
			print "Exception: ".$e->getMessage()."<br />";
			if (trim($e->getMessage() == "Calais Backend-Server is Busy. Please try again later."))
			{
				 if ( ++$retry_count > $max_retry_count )
	  				throw new OpenCalaisException("Retry count exceeded");
	  				
				//print "Error.Will retry after 2s <br />";
				//sleep(2);
				//$this->getTags($link);
			}
		}
	}
	
	private $calaisObj;
	
}

?>
