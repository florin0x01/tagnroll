<?php 
require_once("alchemy/module/AlchemyAPI.php");
require_once("backend.interface.php");
require_once("backendoperations.php");

class AlchemyExtractor implements BackendExtractor
{
	public function __construct()
	{
		if ( !isset($this->alchemyObj) )
		{
			$this->alchemyObj = new AlchemyAPI();
		//	$this->alchemyObj->loadAPIKey("api_key.txt");
			$this->alchemyObj->setAPIKey("98cbe97bd1eb220c8de0d979e7fe77ed3d8e8cad"); 
			$this->relevance_min = 0.4;
		}	
	}
	
	public function setLimitMaxTags($limit)
	{
		$this->maxLimit = $limit;
	}
	
	public function encode($str)
	{
		$temp = split(" ",$str);
		$tempx = join("_", $temp);
		return $tempx;
	}
	
	public function decode($str)
	{
		return str_replace("_", " ", $str);	
	}
	
	public function getTags($link, $encode=false)
	{
	   $returnArray = array();	
	   try{
			$jsonString = $this->alchemyObj->URLGetRankedKeywords($link, AlchemyAPI::JSON_OUTPUT_MODE);		
			$result = json_decode($jsonString);
			
			$ar_relevance = 0;
			$count = 0;
			
			foreach($result->keywords as $partialResult)
			{
				$ar_relevance += $partialResult->relevance;
				$count++;
			}
			
			$ar_relevance = $ar_relevance / $count;
			
	   		foreach($result->keywords as $partialResult)
			{
				if ( $partialResult->relevance >= $ar_relevance && 
						$partialResult->relevance >= $this->relevance_min )
				$returnArray[de_underscore($partialResult->text)] = $partialResult->relevance;
			}
			$returnArray = array_slice($returnArray,0,$this->maxLimit);
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
		return $returnArray;
	}
	
	public function getConceptTags($link)
	{
	  $returnArray = array();
	  try{
			$jsonString = $this->alchemyObj->URLGetRankedConcepts($link, AlchemyAPI::JSON_OUTPUT_MODE);		
			$result = json_decode($jsonString);
			
			foreach($result->concepts as $partialResult)
			{
				$returnArray[de_underscore($partialResult->text)] = $partialResult->relevance;
			}
		}
		catch(Exception $e)
		{
			return $e->getMessage();
		}
		return $returnArray;
	}
	
	private static $alchemyObj;
	private $maxLimit;
	private $relevance_min;
}
?>