<?php
require_once("fluidinfo-php/fluidinfo.php");
require_once("backend.interface.php");

class FluidInfoSetup
{
	protected function __construct()
	{
		$this->_fluid = new Fluidinfo();

		$this->_userId = "tagnroll.com";
		$this->_password = "vl09mbob36dcjplm";

		$this->_fluid->setCredentials($this->_userId, $this->_password);
		$this->UUIDSetup();
		$this->fl = $this->_fluid;

		//TODO : masterSetup should only run ONCE (per session or ?)
//		$this->masterSetup();
	}

	/**
	 *
	 * @return FluidInfoSetup
	 */
	public static function singleton()
	{
		if (!isset(self::$instance))
		{
			$className = __CLASS__;
			self::$instance = new $className;
		}
		
		self::$instance->setupNsNames();
		self::$instance->setupFluidPaths();
		
		return self::$instance;
	}
	 
	public function __get($name)
	{
		if ( isset($this->$name) )
		return $this->$name;
		return NULL;
	}
	 
	protected function masterSetup()
	{
		$this->_categArray = array('Anniversary', 'City', 'Company', 'Continent', 
									'Country', 'Currency', 'EmailAddress', 'EntertainmentAwardEvent', 
									'Facility', 'FaxNumber', 'Holiday', 'IndustryTerm', 
									'MarketIndex', 'MedicalCondition', 'MedicalTreatment', 'Movie', 
									'MusicAlbum', 'MusicGroup', 'NaturalFeature', 'OperatingSystem', 
									'Organization', 'Person', 'PhoneNumber', 'PoliticalEvent', 
									'Position', 'Product', 'ProgrammingLanguage', 'ProvinceOrState', 
									'PublishedMedium', 'RadioProgram', 'RadioStation', 'Region', 
									'SportsEvent', 'SportsGame', 'SportsLeague', 'Technology', 
									'TVShow', 'TVStation', 'URL');
		$this->setupNsNames();
		$this->setupFluidPaths();
		$this->setupNamespaces();
		$this->setupUserTags();
		$this->setupTagsTags();
		$this->setupLinksTags();
		$this->setupPacksTags();
		$this->UUIDSetup();
	}

	protected function UUIDSetup()
	{
		$this->_userUID = $this->_userId. "U_%s";
		$this->_tagUID  = $this->_userId. "T_%s";
		$this->_linkUID = $this->_userId. "L_%s";
		$this->_packUID = $this->_userId. "P_%s";
	}

	protected function setupNamespaces()
	{
		$this->fl->createNamespace($this->_userId, $this->_packsNs , "Packs namespace");
		$this->fl->createNamespace($this->_userId, $this->_usersNs, "Users namespace");
		$this->fl->createNamespace($this->_userId, $this->_tagsNs, "Tags namespace");
		$this->fl->createNamespace($this->_userId+"/"+$this->_tagsNs, $this->_categsNs, "Categories tags ns");
		$this->fl->createNamespace($this->_userId, $this->_linksNs, "Links namespace");
	}

	protected function setupUserTags()
	{
		$this->fl->createTag($this->_userPath, "rating" , "Rating for user", true);
		$this->fl->createTag($this->_userPath, "Username", "Username for the user", true);
		$this->fl->createTag($this->_userPath, "Fullname", "Full name for the user", true);
		$this->fl->createTag($this->_userPath, "Email", "Email for the user", true);
		$this->fl->createTag($this->_userPath, "Password", "Password for the user", true);
		$this->fl->createTag($this->_userPath, "PrivatePacksCount", "Count of private packs", true);
		$this->fl->createTag($this->_userPath, "RatedPacksCount", "How many packs were rated", true);
		$this->fl->createTag($this->_userPath, "CreationDate", "Account creation date", true);
		$this->fl->createTag($this->_userPath, "LastHere", "Last seen here", true);
		$this->fl->createTag($this->_userPath, "PackNames", "List of pack names", true);
	}
	
	protected function setupCategoriesTags()
	{
		//NOTE: IndustryTerm is considered directly as tag, along with SocialTag
		//so the value appears in tags/ namespace and it is also tagged as IndustryTerm
		foreach ($this->_categArray as $category)
			$this->fl->createTag($this->_categPath, $category, "", true);
	}

	protected function setupTagsTags()
	{
		$this->setupCategoriesTags();
	}

	protected function setupLinksTags()
	{
		$this->fl->createTag($this->_linksPath, "rating", "Rating for link", true);
		$this->fl->createTag($this->_linksPath, "votedCount", "Voted count for this link", true);
	}

	protected function setupPacksTags()
	{
		$this->fl->createTag($this->_packPath, "rating", "Rating for this global pack", true);
		$this->fl->createTag($this->_packPath, "votedCount", "Voted count for this pack", true);
	}
	
	protected function setupNsNames()
	{
		$this->_packsNs = "packs";
		$this->_linksNs = "links";
		$this->_usersNs = "users";
		$this->_tagsNs = "tags";
		$this->_categsNs = "categories";
	}
	private function setupFluidPaths()
	{
		$this->_categPath = $this->_userId."/".$this->_tagsNs."/".$this->_categsNs;
		$this->_tagPath= $this->_userId."/".$this->_tagsNs;
		$this->_packPath = $this->_userId."/".$this->_packsNs;
		$this->_userPath = $this->_userId."/".$this->_usersNs;
		$this->_linksPath = $this->_userId."/".$this->_linksNs;
	}

	public function checkResult($result)
	{
		if ( is_array($result) )
			return false;
			
		if (is_object($result))
		{
			//should not happen
			if (count($result->ids) > 1)
				return true;
			if (count($result->ids) == 1)
				return true;
			if (!count($result->ids))
				return false;
		}

		return false;
	}
	 
	public $fl;
	private static $instance;
	protected $_packsNs;
	protected $_usersNs;
	protected $_tagsNs;
	protected $_categsNs;
	protected $_linksNs;
	 
	public $_tagPath;
	public $_linksPath;
	public $_userPath;
	public $_categPath;
	public $_packPath;

	public $_userUID;
	public $_packUID;
	public $_tagUID;
	public $_linkUID;
	
	private $_categArray;

	protected $_fluid;
	protected $_userId;
	protected $_password;
	protected $_lastErrorMessage;
}
?>
