<?php
require_once("backend.interface.php");
require_once("class.user.php");
require_once("class.fluidinfosetup.php");

class FluidInfoUser extends User implements BackendUser
{
	public function __construct()
	{
		$this->fluidSetup = FluidInfoSetup::singleton();
		$this->path = $this->fluidSetup->_userPath;	
		$this->_fluid = $this->fluidSetup->fl;
	}
	
	public function Register()
	{
		//User can only register with spaces, strip off the rest/give warning!
//		print "User Register\n";
		$fl = $this->_fluid;
		$about = sprintf($this->fluidSetup->_userUID, $this->getNick());
		
		//must be unique everytime
//		print "About tag: ".$about;	
		$result = $fl->createObject($about);
		$oid = $result->id;
//		print "Oid: ".$oid."<br />";
		
		$path = $this->path;
	
	
		$fl->tagObject($oid, $path."/rating" , 0.0);
		$fl->tagObject($oid, $path."/Username", $this->getNick());
		$fl->tagObject($oid, $path."/Fullname", $this->getFullName());
		$fl->tagObject($oid, $path."/Email", $this->getEmail());
		$fl->tagObject($oid, $path."/Password", $this->getPassword());
		$fl->tagObject($oid, $path."/PrivatePacksCount", 0);
		$fl->tagObject($oid, $path."/RatedPacksCount", 0);
		$fl->tagObject($oid, $path."/CreationDate", time());
		$fl->tagObject($oid, $path."/LastHere", time());
		
		//$fl->tagObject($id, $tag)t($oid, )
		$encoded_nick = str_replace(" ", "_", $this->getNick());
		$fl->createNamespace($path, $encoded_nick, $this->getNick()."'s namespace");
		$fl->createNamespace($path."/".$encoded_nick, "WebPacks", $this->getNick()."'s webpacks");
		$fl->createTag($path."/".$encoded_nick, "WebPacks", $this->getNick()."'s tag for webpacks", true);	
	}
		
	public function Login()
	{
		$fl = $this->_fluid;
		$path = $this->path."/";
		$query = $path."Username = \"".$this->getNick()."\"";
		$query.= " and ".$path."Password = \"".$this->getPassword()."\"";
		$result = $fl->query($query);
		$result_login = $this->fluidSetup->checkResult($result);
		if ( true == $result_login ) {
			$fl->tagObject($result->ids[0], $path."LastHere", time());
		}
		return $result_login;
	}
	
	public function Exists()
	{
		$fl = $this->_fluid;
		$path = $this->path."/";
		$query = $path."Username = \"".$this->getNick()."\"";
		$result = $fl->query($query);
		return $this->fluidSetup->checkResult($result);
	}
	
	public function Load()
	{
		
	}
	
	private $_fluid;
	private $fluidSetup;
	private $path;
		
}

//$user = BackendFactory::getType("User", "FluidInfo");
?>
