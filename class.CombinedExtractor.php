<?php 
require_once("class.AlchemyExtractor.php");
require_once("class.OpenCalaisExtractor.php");

class CombinedExtractor implements BackendExtractor
{
	public function __construct()
	{
		$this->setLimitMaxTags(5);
	}
	
	public function setLimitMaxTags($limit)
	{
		$this->maxLimit = $limit;
		$this->calais = new OpenCalaisExtractor();
		$this->calais->setLimitMaxTags($this->maxLimit);
		
		$this->alchemy = new AlchemyExtractor();
		$this->alchemy->setLimitMaxTags($this->maxLimit);
	}
	
	public function encode($str)
	{
		$temp = split(" ",$str);
		$tempx = join("_", $temp);
		return $tempx;
	}
	
	public function decode($str)
	{
		return str_replace("_", " ", $str);	
	}
	
	public function getTags($link, $encode=false)
	{
		$tags = array();
		try{
			$tags = $this->calais->getTags($link, $encode);
			if ( !count($tags) )
			{
				$tags = $this->alchemy->getTags($link, $encode);
			}
			
		}catch(OpenCalaisException $e)
		{
				
		}

		$tagsFinal = array();
		foreach($tags as $key=>$value)
		{
			$spTags = explode(" ", $key);
   		     for ($idx=0; $idx<count($spTags); $idx++)
   	   		  {
           		 $spTags[$idx] = strtolower($spTags[$idx]);
				 if(strlen($spTags[$idx]) < 3) continue;
				 $tagsFinal[$spTags[$idx]]= $value;
   	     	}
		}
		return $tagsFinal;
	}
	
	public function getConceptTags($link)
	{
		return array();
	}
	
	private $maxLimit;
	private $calais;
	private $alchemy;
	
}

?>
