<?php
class Link
{
	
	public function __construct($link)
	{
		$this->_linkAddr = $link;
	}	
	
	public function setLinkAddress($link)
	{
		//remove http or https
		$link = str_replace(array('http://','https://'), '', $link);
		
		//remove extra slash from the end
		if ($link[strlen($link)-1] == '/' )
			$link = substr($link, 0, strlen($link)-1);
			
		$this->_linkAddr = $link;
	}
	
	//Setters
	//input: tag and float relevance (2 decimals)
	public function CachePutTag($tag, $relevance)
	{
		if (!count($this->_tagArray))
		{
			$this->_tagArray = array();
		}
		$this->_tagArray[$tag] = $relevance;
	}
	
	public function CacheHasTag($tag)
	{
		return array_key_exists($tag, $this->_tagArray);
	}
	
	public function getLink()
	{
		return $this->_linkAddr;
	}
	
	//Getters
	public function CacheGetTagRelevance($tag)
	{
		if(array_key_exists($tag, $this->_tagArray))
			return $this->_tagArray[$tag];
	}
	
	public function CacheGetAllTags()
	{
		return $this->_tagArray;
	}
	
	private $_linkAddr;
	private $_tagArray;
	
}
?>
