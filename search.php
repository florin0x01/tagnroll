<?php
require_once('fbcheck.php');
if ( isset($_SESSION['state']) ) FbLogin();
?>

<html>
<head>
<title>Tagnroll - automatic tagging</title> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2"> 
<meta name="DESCRIPTION" content="Automatically tag, bookmark and organize your links. Discover a new, fast way to browse YOUR web"> 
<meta name="KEYWORDS" content="automatic tagging, tags, bookmark, search, discover, semantic, organize, roll">
<meta name="robots" content="index,follow"> 
<meta http-equiv="x-ua-compatible" content="IE=9" />

<link rel="Stylesheet" href="css/tags.css" type="text/css" />
<link rel="Stylesheet" href="css/global.css" type="text/css" />
<link rel="Stylesheet" href="css/search.css" type="text/css" />
<link rel="Stylesheet" href="css/history.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript" src="js/jquery.jmodal.1.2.min.js"></script>
<script type="text/javascript" src="js/jquery.masonry.min.js"></script>
<script type="text/javascript" src="js/jquery.ajaxq-0.0.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/tag.js"></script>
<script type="text/javascript" src="js/thumbs.js"></script>

<?php
$lastOid = "";
//Facebook integration: Share button
if ( isset($_GET['roll']) && !empty($_GET['roll']) )
{
$_GET['roll'] = rawurldecode(strtolower($_GET['roll']));
if ( strpos($_GET['roll'], "*") !== false ) exit;
?>
<meta property="og:title" content="A Tagnroll pack" /> 
<meta property="og:description" content="I am glad to share my pack from Tagnroll.com. Here is what I found related to <?php echo $_GET['roll'] ?>" /> 
<meta name="medium" content="mult" />


<script type="text/javascript">
fbShareTag="<?php 
$site = 'http://www.tagnroll.com/search.php?roll='.$_GET['roll'];	
echo "https://www.facebook.com/sharer/sharer.php?u=".urlencode($site)."&t=Tagnroll+-+automatic+tagging&src=sp";  ?> "; <?php
}
?>
</script>


</head>

<body>
<div class="navigation-toolbar">    
    <div class="navigation-button-left"><a href="//www.tagnroll.com" class="navigation-button-link">Home</a></div>
	<div class="navigation-button-middle-draggable"><a class="navigation-button-link-draggable" onclick="alert('In order to start tagging you have to drag me to the bookmarks bar');return false;" href="javascript:void((function(){ var uri = document.URL; window.open('//www.tagnroll.com/search.php?url=' + encodeURI(uri)); })());" title="Tag Me">Tag me</a></div>
    <div class="navigation-button-middle"><a href="//www.tagnroll.com/tagnroll-chrome.crx" class="navigation-button-link">Chrome Plugin</a></div>
	<div class="navigation-button-right"><a href="//www.tagnroll.com/search.php?h" class="navigation-button-link">My Pack</a></div>
<?php
//Facebook integration: Share button
if ( isset($_GET['roll']) && !empty($_GET['roll']) )
{
?>    
    <div class="navigation-button-right"><a href="javascript:void(0)" onClick="window.open(fbShareTag);" class="navigation-button-link">Share on Facebook</a></div>
<?php
}
?>
</div>

<div class="search-toolbar" background="img/tagnrollback.jpg">
<form id="searchform" style="display:inline;"  action="/search.php" method="get">
<input id="search-box" name="roll" size="40px" type="text" />
<button id="search-btn" value="Roll" type="submit">
Roll
</button>
</form>
</div>

<?php include("tagstoolbar.php");?>
<script type="text/javascript">
	$('#tags-toolbar').masonry({
		// options
		isFitWidth :true,
		columnWidth: function( containerWidth ) {
			return containerWidth / 300;
		},
		animated:true
	});
</script>
<div class="index-content" >
<div class="packs-panel" id="packs-panel">
</div>
<div class="links-panel" id="links-panel"></div>

</div>

<?php
if (isset($_GET['h']) )
{
FbLogin();
$x = $_GET['h'];
if ($x==null) $x="0";
?>
<script type="text/javascript">
search("<?php echo $x; ?>");
</script>
<?php } 
else 
if ( isset($_GET['roll']) && !empty($_GET['roll']))
{
?>
<script type="text/javascript">
roll("<?php echo $_GET['roll']; ?>");
</script>
<?php } ?>

</body>

</html>
