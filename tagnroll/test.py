#!/usr/bin/env python

import sys
import signal
import time
import cgi
import cgitb

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *

def onLoadFinished(result):
    if not result:
        print "Request failed"
        sys.exit(1)

    # Set the size of the (virtual) browser window
    webpage.setViewportSize(QSize(1024, 768))
    
    # Paint this frame into an image
    time.sleep(2)
    image = QImage(webpage.viewportSize(), QImage.Format_ARGB32)
   
    painter = QPainter(image)
    webpage.mainFrame().render(painter)
    painter.end()
    image = image.scaled(QSize(320, 240))
    image.save("thumbs/output-%s.jpg" %sys.argv[1][7:].replace("/", "_"), None, 80)
    sys.exit(0)

app = QApplication(sys.argv)
signal.signal(signal.SIGINT, signal.SIG_DFL)

webpage = QWebPage()
webpage.settings().setAttribute(QWebSettings.PluginsEnabled, True)
webpage.settings().setAttribute(QWebSettings.ZoomTextOnly, 2);

webpage.mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)
webpage.mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)
webpage.connect(webpage, SIGNAL("loadFinished(bool)"), onLoadFinished)
webpage.mainFrame().load(QUrl(sys.argv[1]))

sys.exit(app.exec_())

