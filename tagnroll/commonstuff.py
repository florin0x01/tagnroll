import os
import time
import sys
from httplib import HTTP
from urlparse import urlparse

path = '/storage/%s'
 
def checkURL(url):
  try:
   p = urlparse(url)
   h = HTTP(p[1])
   h.putrequest('HEAD', p[2])
   h.endheaders()
   reply = h.getreply()[0] / 100
   print h.getreply()[0]
   if reply == 2 or reply == 3: return 1
   else: return 0
  except:
    print sys.exc_info()[0]
    return 0

def normalizeLink(link):
  return link[7:].replace("\"", "").replace("&", "\&").replace("?", "\?").replace(";", "\;")

def getCurrentDay():
   tm = time.localtime()
   day = "%d-%d-%d"  % (tm.tm_year, tm.tm_mon, tm.tm_mday)
   return day
   
def encodeFile(file):
  return file.replace("\\\\", "_").replace("&", "-").replace("=", "_").replace("?", "_").replace(";", "_").replace("\\", "_")
   
def createDestination(url):
  tmp = path % getCurrentDay()
  link = normalizeLink(url)
  output = link.split('/')
  outputdir = "/".join(output[0:len(output)-1])
  try:
    os.makedirs(tmp + "/" + outputdir)
  except OSError:
    pass
  
 # \\?v=7zOEJ1HdSts\\&amp;feature=related.jpg
  filecomp = encodeFile(output[len(output)-1])
  outputfile = tmp + "/" + outputdir + "/" + filecomp + ".jpg"
  return outputfile
  
def getDestination(url):
  tmp = path % getCurrentDay()
  link = normalizeLink(url)
  output = link.split('/')
  outputdir = "/".join(output[0:len(output)-1])
  filecomp = encodeFile(output[len(output)-1])
  outputfile = tmp + "/" + outputdir + "/" + filecomp + ".jpg"
  return outputfile
  
def cacheHit(url):
  if not os.path.exists(getDestination(url)):
    return False
  return True
