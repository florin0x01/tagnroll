#!/usr/bin/env python
import sys
import signal
import os
import commonstuff

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *

if len(sys.argv) < 2:
  print "<b>Error</b>"
  sys.exit(-1)
     
def onLoadFinished(res):
    if res == False:
      print "<b> Gen. Error </b>"
      	
    # Paint this frame into an image
    image = QImage(webpage.viewportSize(), QImage.Format_ARGB32)
#    image = QImage(320, 240, QImage.Format_ARGB32)
    painter = QPainter(image)
    webpage.mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)
    webpage.mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)
    webpage.mainFrame().render(painter)
    painter.end()
   # qpixmap = QPixmap().fromImage(image)
   # qpixmap2 = qpixmap.copy(0,0,320,240)
   # image = qpixmap2.toImage()
    image = image.scaled(320, 240)
    dest = commonstuff.createDestination(sys.argv[1])
    image.save(dest, None, 80)
    sys.exit(0)

app = QApplication(sys.argv)
signal.signal(signal.SIGINT, signal.SIG_DFL)

webpage = QWebPage()
webpage.settings().setAttribute(QWebSettings.PluginsEnabled, True)
#webpage.settings().setAttribute(QWebSettings.ZoomTextOnly, 2)
# Set the size of the (virtual) browser window
webpage.setViewportSize(QSize(1024, 768))

if len(sys.argv) > 1:
  webpage.connect(webpage, SIGNAL("loadFinished(bool)"), onLoadFinished)
  webpage.mainFrame().load(QUrl(sys.argv[1]))

sys.exit(app.exec_())
