#!/usr/bin/env python
import cgi
import sys
import os
import cgitb; cgitb.enable(display=0, logdir="/tmp")
import commonstuff

print "Content-type: text/html"
print

form = cgi.FieldStorage()

if "url" not in form:
  print "<b>Error</b>"
  sys.exit(-1)

finalUrl = ''

for items in form:
  if items == 'url':
    finalUrl += "%s" % form['url'].value
  else:
    finalUrl += "&%s=%s" %(items, form[items].value)

url = "\"" + finalUrl + "\""

#print url
#sys.exit(0)

#if commonstuff.checkURL(url) == 0:
#  sys.exit(-2)

if commonstuff.cacheHit(url) == False:
  os.system("/usr/bin/xvfb-run -a -s \"-screen 0 1024x768x24\" -e /tmp/xvfb.err.log /usr/bin/python webkit2png-simple.py %s 2>&1 >/tmp/pyth.log "  %url)
  
dest = commonstuff.getDestination(url)
resImg = open(dest, "rb").read().encode("base64")	
print '<img src="data:image/gif;base64,%s" />' %resImg
   
