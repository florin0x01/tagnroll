#!/usr/bin/env python

import cgi
import sys
import os
import cgitb; cgitb.enable(display=0, logdir="/tmp")

print "Content-type: text/html"
print

form = cgi.FieldStorage()

if "url" not in form:
  print "<h1> Error </h1>"
  sys.exit()

url = form["url"].value

os.system("/usr/bin/xvfb-run -a -s \"-screen 0 800x600x24\" -e /tmp/xvfb.err.log /usr/bin/python webkit2png-simple.py %s 2>&1 1>/dev/null "  %url)
dest = "/tmp/output-%s.png" % url[7:].replace("/", "-")
resImg = open(dest, "rb").read().encode("base64")	
print '<img src="data:image/gif;base64,%s" />' %resImg
   
