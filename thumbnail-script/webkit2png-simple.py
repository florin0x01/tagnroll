#!/usr/bin/env python
import sys
import signal

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import *


if len(sys.argv) < 2:
  print "<b>Error</b>"
  sys.exit(-1)

def onLoadFinished(result):
    if not result:
      print "<b>Request failed</b>"
      sys.exit(1)
		  
    # Set the size of the (virtual) browser window
    webpage.setViewportSize(QSize(1024, 768))

    # Paint this frame into an image
    image = QImage(webpage.viewportSize(), QImage.Format_ARGB32)
#    image = QImage(320, 240, QImage.Format_ARGB32)
    painter = QPainter(image)
    webpage.mainFrame().setScrollBarPolicy(Qt.Horizontal, Qt.ScrollBarAlwaysOff)
    webpage.mainFrame().setScrollBarPolicy(Qt.Vertical, Qt.ScrollBarAlwaysOff)
    webpage.mainFrame().render(painter)
    painter.end() 
    image = image.scaled(640, 480)
    dest = "/tmp/output-%s.png" % sys.argv[1][7:].replace("/", "-")  
    image.save(dest)
    sys.exit(0)

app = QApplication(sys.argv)
signal.signal(signal.SIGINT, signal.SIG_DFL)

webpage = QWebPage()
webpage.settings().setAttribute(QWebSettings.PluginsEnabled, True)
if len(sys.argv) > 1:
  webpage.connect(webpage, SIGNAL("loadFinished(bool)"), onLoadFinished)
  webpage.mainFrame().load(QUrl(sys.argv[1]))

sys.exit(app.exec_())
