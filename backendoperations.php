<?php
session_start();
if ( !isset($_SESSION['uid']) ) $_SESSION['uid'] = 'tagnroll';
require_once("class.backend.factory.php");
require_once("fluidinfo-php/fluidinfo.php");

function getAvailableExtractors()
{
	return array("Alchemy", "OpenCalais", "Combined");
}

function getAvailableParsers()
{
	return array("Youtube");
}

function de_underscore($item)
{
	$temp = explode("_", $item);
	$item = join(" ", $temp);
	return $item;
}

function underscore($item)
{
	$temp = explode(" ", $item);
	$item = join("_", $temp);
	return $item;
}

function stripLink($site)
{

	if ( stripos($site, "youtube.com") !== false )
	{
		if (strpos($site, "&list=") !== false)
			$site = substr($site, 0, strpos($site, "&list="));
	}

	return $site;
}

function addLink($site)
{
	try {
	//TODO check if the link really exists before adding it to FluidInfo ?
		$site = stripLink($site);
		$link = BackendFactory::getType("Link", "FluidInfo");
		$link->setLinkAddress($site);
		return $link->Add();
	}catch(HttpInvalidParamException $e)
	{
		//Fluidinfo might be down
		print "Backend is down. Please try again later";
		exit;
	}
}

function getPacks()
{
	$fluidSetup = FluidInfoSetup::singleton();
	$fl = $fluidSetup->fl;
	$arr = $fl->getNamespace("tagnroll.com/users/".$_SESSION['uid']."/WebPacks", false, false, true);
	return $arr->tagNames;
}

function addPack($query, $tags)
{
	$fluidSetup = FluidInfoSetup::singleton();
	$path = $fluidSetup->_linksPath;
	$fl = $fluidSetup->fl;
	$fl->updateValues($query, $tags);
//	$fl->sendTaggedObject();
}

function addTag($path, $tag)
{
	$fluidSetup = FluidInfoSetup::singleton();
	$fl = $fluidSetup->fl;
	$fl->createTag($path, $tag, "", true);
//	$fl->sendTaggedObject();
}

function addTags($site, $oid, $arr)
{
	$link = BackendFactory::getType("Link", "FluidInfo");
	$link->setLinkAddress($site);
	$link->setOid($oid);

	foreach($arr as $key=> $value)
	{
		$spTags = explode(" ", $key);
		for ($idx=0; $idx<count($spTags); $idx++)
		{
			$spTags[$idx] = strtolower($spTags[$idx]);
			$path = "tagnroll.com/tags";
			$val = (int)(round($value,2) * 100);
			$link->AddTag($path, $spTags[$idx], "");
			$link->AddTag($path."/relevance", $spTags[$idx], $value);
		}
	}

	$link->AddTag("tagnroll.com/users", $_SESSION['uid'], "");
	$link->SendTaggedObject();
	
}

function extractTags($link, $backendExtractor, $prefer_parser=true, $maxTags=5)
{
	$results = array();
	if ( $prefer_parser == true )
	{
		$pars = getAvailableParsers();
		foreach($pars as $parsers)
		{
//			if ( stripos($link, $parsers) !== false )
			{
				$extractor = BackendFactory::getType("Parser", $parsers);
				$extractor->setLimitMaxTags($maxTags);
				$results = $extractor->getTags($link, false);
				if ( !count($results) ) 
					continue;
				else 
					return $results;
			} 
		}
	}
	
	$ext = getAvailableExtractors();
	$found = "";
	foreach($ext as $extractor)
	{
		if ( !strcasecmp($backendExtractor, $extractor) ) 
		{
			$found = $extractor;
			break;
		}
	}
	if ( $found == "" ) throw new Exception("No available extractors by the name ".$backendExtractor);
	
	try 
	{
		$extractor = BackendFactory::getType("Extractor", $extractor);
		$extractor->setLimitMaxTags($maxTags);
		$arr = $extractor->getTags($link, false);
	}
	catch(OpenCalaisException $e)
	{
		//do something with the exception
		return $arr;
	}

	return $arr;
}

?>
