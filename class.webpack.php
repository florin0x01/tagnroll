<?php
class WebPack
{
	public function __construct($name="")
	{
		$this->_name = $name;
		$this->_private = false;
	}
	
	public function Add($name)
	{
		$this->_linksArr[] = $name;
	}
	
	public function setName($name)
	{
		$this->_name = $name;
	}
	
	public function getName()
	{
		return $this->_name;
	}
	
	public function isPrivate()
	{
		return ($this->_private==true);
	}
	
	public function canView($userName)
	{
		return array_key_exists($userName, $this->_userACL)	;
	}
	
	private $_name;
	private $_userACL;
	private $_private;
	private $_mainTopics;
	private $_linksArr;
}
?>