<?php
require_once("class.user.php");
require_once("class.link.php");

interface BackendUser
{
	//User management stuff
	public function Register();
	public function Exists();
	public function Login();
	public function Load();
}

interface BackendLink
{
	//Link stuff
	public function Exists();
	public function Add();
	public function AddTag($path, $tag, $value);
	public function HasTag($tag);
	public function Rate($rating);
	public function Load();
//	public static function getUsers();
}

interface BackendTag
{
	
}

interface BackendExtractor
{
	public function getTags($link, $encode=true);
	public function getConceptTags($link); //returns array Key-Value
	public function setLimitMaxTags($limit);
	public function encode($str);
	public function decode($str);
}
	
?>
