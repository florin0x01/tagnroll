<?php
class User
{
	public function __construct($name)
	{
		$this->_nick = $name;
		$this->_privatePacksCount = 0;
		$this->_lastTimeHere = 0;
		$this->_ratedPacks = 0;
		$this->_creationDate = "";
	}
	
	public function setEmail($email)
	{
		$this->_email = $email;
	}
	
	public function setFullName($name)
	{
		$this->_fullName = $name;
	}
	
	public function setNick($n)
	{
		$this->_nick = $n;
	}
	
	public function setPassword($password)
	{
		$this->_password = sha1($password, false);
	}
	
	public function incrementRatedPacks()
	{
		$this->_ratedPacks++;
	}
	
	public function incrementPacksCount()
	{
		$this->_privatePacksCount++;
	}
	
	public function getRatedPacksCount()
	{
		return $this->_ratedPacks;
	}
	
	public function getEmail()
	{
		return $this->_email;
	}
	
	public function getFullName()
	{
		return $this->_fullName;
	}
	
	public function getPassword()
	{
		return $this->_password;
	}
	
	public function getNick()
	{
		return $this->_nick;
	}
		
	private $_nick;
	private $_userName;
	private $_fullName;
	private $_email;
	private $_password;
	private $_privatePacksCount;
	private $_ratedPacks;
	
	private $_creationDate;
	private $_lastTimeHere;
	
}
?>