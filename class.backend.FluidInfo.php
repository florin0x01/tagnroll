<?php
/**
 * 
 * Schema description
 * @author nini
 *
 * 
USERS NAMESPACE
-- applied to one User object ! --

/users/rating -> (1-10)
/users/UserName -> John86
/users/FullName -> value
/users/Email -> value
/users/Password -> value
/users/PrivatePacksCount -> value
/users/RatedPacksCount -> value
/users/CreationDate -> value
/users/LastHere -> value
 
-- applied to Links objects ! --
/users/John86 ?!
/users/John86/WebPacks - TAG (specifies that the link is part of a webpack of John86)
/users/John86/WebPacks/GigiPack - TAG (specifies that the link is part of the GigiPack webpack created by John86)
/users/John86/WebPacks/GigiPack/private - boolean
/users/John86/WebPacks/GigiPack/ACL - SET (specifies a list of users who can see this pack)
/users/John86/WebPacks/GigiPack/rating (1-10)
/users/John86/WebPacks/GigiPack/votedCount

TAGS NAMESPACE
/tags/Food -> relevance (0.85)  - Applied to Link objects
/tags/Food/rating -> (1-10) - Rate the food tag - optional, applied to link objects
/tags/Food/votedCount

LINKS NAMESPACE
/links/www.slashdot.org -> description ? Applied to Link objects (Combo of one link + n tags / object)
/links/rating -> (1-10)


PACKS NAMESPACE (Global, special, system wide webpacks)
/packs/PackName -> Value
/packs/rating (1-10)

*/

?>