<html>
<head>
    <title>Tagnroll - Automatic tagging</title>
    <link rel="Stylesheet" href="css/global.css" type="text/css" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2"> 
    <meta name="DESCRIPTION" content="Automatically tag, bookmark and organize your links. Discover a new, fast way to browse YOUR web">
    <meta name="KEYWORDS" content="automatic tagging, tags, bookmark, search, discover, semantic, organize, roll">
    <meta name="robots" content="index,follow">
</head>

<body>

<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '151575538293956', // App ID
      channelUrl : '//www.tagnroll.com/channel.php', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });
  };

  // Load the SDK Asynchronously

  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;

     js.src = "//connect.facebook.net/en_US/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
     
   }(document));

</script>

<div class="navigation-toolbar"> 
    <div class="navigation-button-left"><a href="#about" class="navigation-button-link" onclick="alert('Tag links for further reading and share them with your friends.'); return true;">What's about?</a></div>
	<div class="navigation-button-middle-draggable"><a class="navigation-button-link-draggable" onclick="alert('In order to start tagging you have to drag me to the bookmarks bar');return false;" href="javascript:void((function(){ var uri = document.URL; window.open('//www.tagnroll.com/search.php?url=' + encodeURI(uri)); })());" title="Tag Me">Tag me</a></div>
    <div class="navigation-button-middle"><a href="//www.tagnroll.com/tagnroll-chrome.crx" class="navigation-button-link">Chrome Plugin</a></div>
	<div class="navigation-button-right"><a href="//www.tagnroll.com/search.php?h" class="navigation-button-link">My Pack</a></div>
</div>

<div class="search-toolbar">
<form id="searchform" action="/search.php" style="display:inline;" method="get">
<div>
<input id="search-box" name="roll" size="40px" type="text" />
<button id="search-btn" value="Roll" type="submit">
Roll
</button>
<div>
</form>
</div>
<!--<div class="index-content" >
<div class="fb-login-button" data-show-faces="true">Login with Facebook</div>
</div>
<p style="text-align:center; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size:8pt">
<b>Start using Tagnroll</b></br>
Drag 'Tag me' button to the bookmarks bar</br>
Go to the webpage you want to tag (English only). You can also try Youtube.</br>
Click on 'Tag me' button</br>
And you are done! You tagged your first page</br>
You can see all your links and tags in 'My Links' section or you can search them by keywords</br>
</p>-->
</body>
</html>
