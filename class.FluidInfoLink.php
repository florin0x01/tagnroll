<?php 
require_once("backend.interface.php");
require_once("class.link.php");
require_once("class.fluidinfosetup.php");

function getTagsOnly($val)
{
        return (strpos($val, "tags/relevance") === false && strpos($val, "tagnroll.com/links") === false
                        && strpos($val, "fluiddb/about") === false && strpos($val, "tags/TagVector") === false
                        && strpos($val, "tagnroll.com/users") === false && strpos($val, "categories/") === false && strpos($val, "/WebPacks") === false ) ;
}

function getUsersOnly($val)
{
        return (strpos($val, "tags/relevance") === false && strpos($val, "tagnroll.com/links") === false
                        && strpos($val, "fluiddb/about") === false && strpos($val, "tags/TagVector") === false
                        && strpos($val, "tagnroll.com/tags") === false && strpos($val, "categories/") === false && strpos($val, "/WebPacks") === false)  ;
}


function removeUnderscore($val)
{
        $val = str_replace("_", " ", $val);
        $val = str_replace("tagnroll.com/tags/", "", $val);
        return $val;
}

class FluidInfoLink extends Link implements BackendLink
{
	public function __construct()
	{
		$this->fluidSetup = FluidInfoSetup::singleton();
		$this->path = $this->fluidSetup->_linksPath;
		$this->_fluid = $this->fluidSetup->fl;
	}

	public static function getLinkByOid($oid)
	{
		$setup = FluidInfoSetup::singleton();
        $path = $setup->_linksPath;
        $fluid = $setup->fl;
        return $fluid->getObjectTag($oid, "tagnroll.com/links/link");
	}

	public static function searchOids($query, $oid="")
	{
		$setup = FluidInfoSetup::singleton();
        $path = $setup->_linksPath;
        $fluid = $setup->fl;
        $x = $fluid->query($query);
		error_log(print_r($x, true));	
		$arr = array();
		 foreach($x->ids as $key=>$val)
			 if ( $val != "22777766-9602-4ba9-9ad3-b5bc9fa49967") $arr[] = $val;
		 if ( $oid != "" && array_search($oid, $x->ids) === false)
        {
            //$obj = $fluid->getObject($oid);
            if ( $oid != "22777766-9602-4ba9-9ad3-b5bc9fa49967" )
                $arr[] = $oid;
        }
        return $arr;

  	}

	public static function search($query, $oid="")
	{
		$setup = FluidInfoSetup::singleton();
		$path = $setup->_linksPath;
		$fluid = $setup->fl;
		$x = $fluid->query($query);
		
		$linkArray = array();
		if ( count($x->ids) )
		{
			foreach($x->ids as $key=>$val)
				if ( $val != "22777766-9602-4ba9-9ad3-b5bc9fa49967") $linkArray[$val] = $fluid->getObjectTag($val, "tagnroll.com/links/link");
		}

		if ( $oid != "" && array_search($oid, $x->ids) === false) 
		{
			//$obj = $fluid->getObject($oid);
			if ( $oid != "22777766-9602-4ba9-9ad3-b5bc9fa49967" )
				$linkArray[$oid] = $fluid->getObjectTag($oid, "tagnroll.com/links/link");
		}
		return $linkArray;
	}
	
	public static function listObjects($userId)
	{
		$setup = FluidInfoSetup::singleton();
		$path = $setup->_linksPath;
		$fluid = $setup->fl;
		$query = "has tagnroll.com/users/".$userId;
		$x = $fluid->query($query);
		
		$oidArray = array();
		if ( count($x->ids) )
		{
			foreach($x->ids as $key=>$val)
				if ( $val != "22777766-9602-4ba9-9ad3-b5bc9fa49967") $oidArray[] = $val;
		}
		
		return $oidArray;	
	}	
	
	public static function getURL($linkOid)
	{
		$setup = FluidInfoSetup::singleton();
		$path = $setup->_linksPath;
		$fluid = $setup->fl;
		return $fluid->getObjectTag($linkOid, "tagnroll.com/links/link");
	}
	
	public static function getTags($linkOid, $remove=true)
	{
		$setup = FluidInfoSetup::singleton();
		$path = $setup->_linksPath;
		$fluid = $setup->fl;
		$res = $fluid->getObject($linkOid);

		foreach($res->tagPaths as $key=>$value) $ret[] = $value;	
	
		if($remove==true)
		{
			$ret = array_filter($ret, "getTagsOnly");
	        $ret = array_map("removeUnderscore", $ret);
		}
		return $ret;
	}

	public static function deleteTag($linkOid, $tag)
	{
		 $setup = FluidInfoSetup::singleton();
        $path = $setup->_linksPath;
        $fluid = $setup->fl;
       // $res = $fluid->getObject($linkOid);
		$fluid->deleteObjectTag($linkOid, $tag);
					

	}	
	
	public function Exists()
	{
		
	}
	
	function isValidURL($url)
	{
		$file      = @fopen($url,"r"); 
		$status    = -1; 

		if (!$file) 
		{		
			$status = -1;  // Site is down 
		} 
		else 
		{ 
			$status = 1; 
			fclose($file); 
		} 
		
		return $status; 
	}

	public function Add()
	{
		$fl = $this->_fluid;
		
		//validate link
		if (strlen($this->getLink()) == 0 || !$this->isValidURL($this->getLink()))
		{
			return false;
		}
		
		$about = sprintf($this->fluidSetup->_linkUID, $this->getLink());
		
		//must be unique everytime
		//print "About tag: ".$about;
		$result = $fl->createObject($about);
		$this->oid = $result->id;
	
		$path = $this->path."/";
		//print "Path: ".$this->path."<br />";
		$fl->createTag($this->path, $this->getLink(), $this->getLink(). " link tag", true);
		
		
		$fl->tagObject($this->oid, $path."link", $this->getLink());
		$fl->tagObject($this->oid, $path.$this->getLink(), "");
		$fl->tagObject($this->oid, $path."rating", 0.0);
			
		return $this->oid;
	}
	
	public function setOid($oid)
	{
		$this->oid = $oid;
	}
	
	public function HasTag($tag)
	{
		
	}

	public static function getUsers($id)
	{
		$setup = FluidInfoSetup::singleton();
//		$path = $setup->_linksPath;
		$fluid = $setup->fl;

		$res = $fluid->getObject($id);
		foreach($res->tagPaths as $key=>$value) $ret[] = $value;	
		$ret = array_filter($ret, "getUsersOnly");
	        $ret = array_map("removeUnderscore", $ret);

		$users = array();
		
		foreach($ret as $fbid)
		{
			$fbid = str_replace("tagnroll.com/users/","",$fbid);
			//error_log("Fbid: ".$fbid);
			$q = "tagnroll.com/users/Username=\"".$fbid."\" ";
			//error_log($q);
			$oid = $fluid->query($q);
			//error_log(print_r($oid,true));
			$users[$fbid] = $fluid->getObjectTag($oid->ids[0], "tagnroll.com/users/Fullname");			
			
		}
		//error_log(print_r($users, true));
		return $users;
	}
	
	public function AddTag($path, $tag, $value)
	{
		$fl = $this->_fluid;
		$id = $this->oid;
		//print "Tag: ".$tag." value ".$value."<br />";
		$fl->tagObject($id, $path."/".$tag, $value);
	}
	
	public function SendTaggedObject()
	{
		$fl = $this->_fluid;
		$fl->sendTaggedObject();
	}
	
	public function printCache()
	{
		$fl = $this->_fluid;
		$fl->printCache();
	}
	
	public function Rate($rating)
	{
		
	}
	
	public function Load()
	{
		
	}
	
	private $_fluid;
	private $fluidSetup;
	private $path;
	private $oid;
}

?>
