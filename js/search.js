var fbShareTag = '';
var curIdx = 0;
var linksData = [];
var _links = [];
var linksPerPage=0;
var _isHistory = false;

var sz = function(data) {
        var count=0;
        for(var e in data) count++;
        return count;
};

//Initialization code
$(function() {
	//adds StartWith function for strings
	if (typeof String.prototype.startsWith != 'function') {
		  String.prototype.startsWith = function (str){
		    return this.indexOf(str) == 0;
		  };
	}
	
	$("#search-box").keyup(function(event){
	    if(event.keyCode == 13){
	        $("#search-btn").click();
	    }
	});
	
	linksPerPage= Math.round($(window).height()/320 + 3);

	if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)) {
		window.onscroll = function () {
			if ($(document).height() - $(window).scrollTop() <= 1278) {
				// At the moment only works on iPhone
				if ( sz(linksData)  > 0 )  { curIdx+=linksPerPage; showLinks(linksData,curIdx); }				
			}
		}
    }
	else
	$(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
          // lastPostFunc();	
		  if ( sz(linksData)  > 0 )  { curIdx+=linksPerPage; showLinks(linksData,curIdx); }
        }
	});

});

function roll(input)
{
	$("#search-box").val(input);
	search("");	
}

function validateInput(str)
{
	while(str.indexOf('  ')!=-1) str=str.replace('  ',' ');
	if ( str.length < 3 )
	{
		return false;
	}
	
	var a = str.split(" ");
	for(var i in a)
	{
		if ( a[i].length < 3 )
		{
			return false;
		}
	}	
	
	return true;
}

function permute(v, m)
{ //v1.0
    for(var p = -1, j, k, f, r, l = v.length, q = 1, i = l + 1; --i; q *= i);
    for(x = [new Array(l), new Array(l), new Array(l), new Array(l)], j = q, k = l + 1, i = -1;
        ++i < l; x[2][i] = i, x[1][i] = x[0][i] = j /= --k);
    for(r = new Array(q); ++p < q;)
        for(r[p] = new Array(l), i = -1; ++i < l; !--x[1][i] && (x[1][i] = x[0][i],
            x[2][i] = (x[2][i] + 1) % l), r[p][i] = m ? x[3][i] : v[x[3][i]])
            for(x[3][i] = x[2][i], f = 0; !f; f = !f)
                for(j = i; j; x[3][--j] == x[2][i] && (x[3][i] = x[2][i] = (x[2][i] + 1) % l, f = 1));
    return r;
};

function search(uid)
{
	_isHistory = false;
	$("#tags-toolbar").empty();
	
	res = document.getElementById('search-box');
	str = res.value;
	str = str.toLowerCase();	

	fbShareTag = 'http://www.facebook.com/sharer/sharer.php?u=' + encodeURI('http://www.tagnroll.com/search.php?roll=' + str);
	fbShareTag = fbShareTag + '&t=Tagnroll+-+automatic+tagging&src=sp';	
		
	if (!validateInput(str) && uid=="")
	{
		$("#links-panel").empty();
		$("#links-panel").append('<div style="font-family:Verdana; font-size:8pt; font-color:gray">Search at least 3 chars.</div>');			
			return;
	}
	
	var strTokens = str.split(" ");
	permuteStr = permute(strTokens);
	
	permuteStrJSON = JSON.stringify(permuteStr);
	var ctrlUrl = '//www.tagnroll.com/controller/controller.search.php';

	if (uid != "")
	{	
		_isHistory = true;
		ctrlUrl = ctrlUrl + '?h=' + uid;	
	}
	
	$.ajax({
		  type: 'POST',
		  url: ctrlUrl,
		  data: permuteStrJSON,
		  success: searchCompleted,
		  error: searchQueryError
		});
			
}

function searchCompleted(data)
{
	curIdx=0;
	linksData = data;
	showLinks(linksData,curIdx);
}

function searchQueryError(data)
{
	$("#links-panel").empty();
	$("#links-panel").append('<div style="font-family:Verdana; font-size:8pt; font-color:gray">backend is down. Please try again later.</div>');
}

var tagCounter = -1;
var usersCounter = -1;
var counter=-1;

function deletelinkSuccess(data) { alert("Link was untagged."); }
function deletelinkErr(data) { console.log("Error deleting link" + data); }

function deleteLink(linkOid, parentId)
{

	var confirmed = window.confirm("Untag this link?");

	if (confirmed)
	{
		$("#history-tile" + parentId).remove();
		$("#links-panel").masonry( 'reload');
		
		$.ajax({	type: 'POST',
		 		 url: '//www.tagnroll.com/controller/controller.deletelink.php',
		 		 data: JSON.stringify(linkOid),
		 		 success: deletelinkSuccess,
		  		error: deletelinkErr
				});
				
	} 				
}

function loadTags(data)
{
	tagCounter++;
	var currentLinkOid = _links[tagCounter];
	
	$("#history-tile" + tagCounter).prepend('<div id="historytags' + tagCounter +'"  class="history-tags"></div>');
	if (_isHistory)
	{
		$("#history-tile" + tagCounter).prepend('<div style="text-align: right; margin: 5px;"><img onclick="deleteLink(\'' + currentLinkOid + '\', \''+ tagCounter +'\')" src="img/delete.png"></div>');
	}
	for(var i in data) 
	{
		$("#historytags"+tagCounter).append('<div onClick="window.open(\'search.php?roll='+data[i]+'\')'+'" class="history-tag">' + data[i]+ '</div>'); 
	}
	$("#links-panel").masonry( 'reload');
}

function loadTagsError(data)
{
tagCounter++;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function loadUsers(data)
{
	usersCounter++;

	if (Object.size(data) == 0)
		return;
	
	$("#history-tile" + usersCounter).append('<div id="userslist' + usersCounter +'"  class="history-tags"></div>');
	for(var i in data) 
	{
		
		if (i == 'null')
			continue;

		$("#userslist"+ usersCounter).append('<div onClick="window.open(\'search.php?h='+i+'\')'+'" class="history-tag">' + data[i]+ '</div>'); 
	}

	$("#links-panel").masonry( 'reload');

}

function loadUsersError(data)
{
usersCounter++;
}



function showLinks(links, curIdx) 
{  

_links = links;
	  if (curIdx==0) $("#links-panel").empty();
	 
	 	$('#links-panel').masonry({
		// options
		itemSelector : '.history-tile',
		isFitWidth :true,
		isAnimated:false
	});

	 temp = JSON.stringify(links);

	  for (var i=curIdx; i < curIdx + linksPerPage && i < sz(links); i++)	 
  	 {
		 $.ajaxq("linksQueue", {
          type: 'POST',
          url: '//www.tagnroll.com/controller/controller.getlink.php',
          data: JSON.stringify(links[i]),
          success: function(link) {
			counter++;	
				if (link == false)
				{
				//	$("#links-panel").append('<div style="font-family:Verdana; font-size:8pt; font-color:gray">Oops. You didn\'t tag anything about this. Install the Chrome plugin to start tagging</div>').masonry( 'reload');

					link="";
				}
				
	    		var clink = 'http://' + link;
	   		    var cont = "";

				if (link.indexOf("www.youtube.com") != 0)
				{
	    				cont = "<div class=\"history-tile\" id=\"history-tile"+counter+"\"><div id=\"resizable" + counter + "\" class=\"link-thumb\">" + getSiteThumb(link) + "</div></div>";
				}
				else 
				{			
						cont = "<div class=\"history-tile\" id=\"history-tile"+counter+"\"><div id=\"resizable" + counter + "\" class=\"link-thumb\">" + getYoutubeContent(link) + "</div></div>";			    
				}
						   
			   $("#links-panel").append(cont);
			   
			   $.ajaxq("tagsQueue",{
		 		 type: 'POST',
		 		 url: '//www.tagnroll.com/controller/controller.gettag.php',
		 		 data: JSON.stringify(links[counter]),
		 		 success: loadTags,
		  		error: loadTagsError
				});

			  $.ajaxq("usersQueue", {
				  type: 'POST',
		  		  url: '//www.tagnroll.com/controller/controller.getusers.php',
		  		  data: JSON.stringify(links[counter]),
		 		 success: loadUsers,
		 		 error: loadUsersError
				});
	
				$("#links-panel").masonry( 'reload');
				
			},
		  error: function(data) { counter++; }
        });

	}



	
 }




