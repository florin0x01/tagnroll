var tagsArray = new Object();

var queryText = "";

function tagChange(id)
{
	queryText = "";
	if ( tagsArray[id] == null) tagsArray[id] = true;
	
	else if ( tagsArray[id] == true ) 
		tagsArray[id] = false;
	else	
		tagsArray[id] = true;
		
		
	arr = new Array();
	
	for (var i in tagsArray )
	{
		if ( tagsArray[i] == true )
		{
			//alert(i+'_label');
			$("#"+i).css("font-weight", "bold");
			var obj = document.getElementById(i);
			var content =  obj.innerText || obj.textContent;
			var x= content.replace('\n', '');
			arr.push(x);
			
			queryText += x + " ";
		}
		else 
		{
			$("#"+i).css("font-weight", "normal");
		}
	}	

	queryText = queryText.replace(/^\s*/, "").replace(/\s*$/, "");
	$("#search-box").val(queryText);
}

function showLinkPicture(url, parentId)
{
	if (url.indexOf("www.youtube.com") != 0)
	{
			$('#' + parentId).append("<div class=\"link-thumb\" style=\" margin:0px; margin-left: 5px;\">" + getSiteThumb(url) + "</div>");	
	}
	else
	{		
			$('#' + parentId).append("<div class=\"link-thumb\" style=\" margin:0px; margin-left: 5px;\">" + getYoutubeContent(url) + "</div>");		
	}	
}
