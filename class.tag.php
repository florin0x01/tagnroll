<?php
class Tag
{
	public function __construct($tag="")
	{
		$this->_tag = $tag;
	}
	
	public function setTag($tag)
	{
		$this->_tag = $tag;
	}
	
	//relevance - 0.1 to 1.0
	public function setRelevance($relevance)
	{
		$this->_relevance = $relevance;
	}
	
	public function getRelevance()
	{
		return $this->_relevance;
	}
	
	public function getTag()
	{
		return $this->_tag;
	}
	
	private $_tag;
	private $_relevance;
}
?>