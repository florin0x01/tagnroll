<?php
require_once('fbcheck.php');
FbLogin();
?>

<html>
<head>
<title>Tagnroll - automatic tagging</title> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2"> 
<meta name="DESCRIPTION" content="Automatically tag, bookmark and organize your links. Discover a new, fast way to browse YOUR web"> 
<meta name="KEYWORDS" content="automatic tagging, tags, bookmark, search, discover, semantic, organize, roll">
<meta name="robots" content="index,follow"> 
<meta http-equiv="x-ua-compatible" content="IE=9" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes" />

<link rel="Stylesheet" href="css/global.css" type="text/css" />
<link rel="Stylesheet" href="css/history.css" type="text/css" />
<link rel="Stylesheet" href="css/search.css" type="text/css" />

<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript" src="js/jquery.jmodal.1.2.min.js"></script>
<script type="text/javascript" src="js/jquery.masonry.min.js"></script>
<script type="text/javascript" src="js/jquery.ajaxq-0.0.1.js"></script>
<script type="text/javascript" src="js/search.js"></script>
<script type="text/javascript" src="js/thumbs.js"></script>
<script type="text/javascript" src="js/history.js"></script>

</head>

<body>
<div class="navigation-toolbar">    
    <div class="navigation-button-left"><a href="http://www.tagnroll.com" class="navigation-button-link">Home</a></div>
	<div class="navigation-button-middle-draggable"><a class="navigation-button-link-draggable" onclick="alert('In order to start tagging you have to drag me to the bookmarks bar');return false;" href="javascript:void((function(){ var uri = document.URL; window.open('http://www.tagnroll.com/search.php?url=' + encodeURI(uri)); })());" title="Tag Me">Tag me</a></div>
    <div class="navigation-button-middle"><a href="http://tagnroll.com/tagnroll-chrome.crx" class="navigation-button-link">Chrome Plugin</a></div>
	
</div>

<div class="search-toolbar" >
<form id="searchform" style="display:inline;"  action="/search.php" method="get">
<input id="search-box" name="roll" size="40px" type="text" />
<button id="search-btn" value="Roll" type="submit">
Roll
</button>
</form>
</div>

<?php include("history_widget.php"); ?>

</body>

</html>
