<?php 
require_once("fluidinfo-php/fluidinfo.php");
require_once("../class.FluidInfoLink.php");
session_start();
header('Content-Type: application/json');

require_once("../class.backend.factory.php");

$response = array('Succes' => '1');
$linkArray = array();

$oid = json_decode(file_get_contents("php://input"), true);

$f = fopen("/tmp/query-gettags.txt", "wt");

if ( $oid == "" )
{
	echo json_encode($oid, JSON_FORCE_OBJECT);
	exit;
}

fprintf($f, "%s\n", print_r($oid, true));
$tags = FluidInfoLink::getTags($oid);

$res = json_encode($tags, JSON_FORCE_OBJECT);

fclose($f);

echo $res;

?>
