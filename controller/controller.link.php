<?php

session_start();
$time_start = microtime(true);
if (! isset($_SESSION['uid']) )
{
	print "Please login to continue <br />";
	exit;
}
if ( !isset($_POST['site']) || empty($_POST['site']) )
{
	print "Fill in all fields <br />";
	exit;
}
require_once("../class.backend.factory.php");

//$site = substr($_POST['site'], strpos($_POST['site'],"/")+2);

//TODO: skip the last / from the URL if any
//TODO: normalize URL and tags ! Also search for special chars
$site = $_POST['site'];

//TODO check if the link really exists before adding it to FluidInfo


$link = BackendFactory::getType("Link", "FluidInfo");
$link->setLinkAddress($site);
$link->Add();

print "Site: ".$site."<br />";
print "Site: ".$_POST['site']."<br />";

$extractor = BackendFactory::getType("Extractor", "Combined");
$extractor->setLimitMaxTags(5);


//TODO add user tags
$t_extract = microtime(true);
try {
	$arr = $extractor->getTags($_POST['site']);

	
	$t_end = microtime(true);
	print "Term extraction: ". ($t_end - $t_extract)."<br />";
	
	$final_str = "";
	
	
	
	foreach($arr as $key=> $value)
	{
		$path = "tagnroll.com/tags";
		$link->AddTag($path, $key, "");
		$link->AddTag($path."/relevance", $key, $value);
		$val = (int)(round($value,2) * 100);
		$final_str.=$key.$val;
	}
	print "str: ".$final_str;
	
	/*
	foreach($arr as $key=>$value)
	{
		//	print "<pre>";
		//	print_r($value);
		//	print "</pre>";
		foreach($value as $key2=>$value2)
		{
			if ($key=="tags")
			{
				$path = "tagnroll.com/".$key;
				$link->AddTag($path, $key2, "");
				$link->AddTag($path."/relevance", $key2, $value2);
				$final_str.=$key2.$value2;
				$final_str.= " ";
				
			}
			else {
				$path = "tagnroll.com/tags/categories";
				$link->AddTag($path, $key, $key2);	
				$link->AddTag("tagnroll.com/tags",$key2, $key);
				$link->AddTag("tagnroll.com/tags/relevance", $key2, $value2);
				$final_str.=$key.$key2;
				$final_str.=" ";
			}
			
		}
	}*/
	
	
}
catch(OpenCalaisException $e)
{
	print "An error has occurred: ".$e->getMessage(). "<br />";
}
$time_end = microtime(true);
print "Script duration: ".($time_end-$time_start);
?>