<?php 
require_once("fluidinfo-php/fluidinfo.php");
require_once("../class.FluidInfoLink.php");
require_once("../backendoperations.php");
session_start();
header('Content-Type: application/json');
if (! isset($_SESSION['uid']) )
{
	echo json_encode("Please login to continue");
	exit;
}

require_once("../class.backend.factory.php");

$response = array('Succes' => '1');
$f = fopen("/tmp/pack.txt", "wt");

$basepath = "tagnroll.com/users/".$_SESSION['uid'];

if ( isset($_REQUEST['getPackContent']) )
{
	$pack = json_decode(file_get_contents("php://input"), true);
	$query = "has ".$basepath."/WebPacks/".$pack;
	fprintf($f, "%s\n", $query);
	$linkArray = FluidInfoLink::search($query);
	fprintf($f, "%s\n", print_r($query, true));
	fprintf($f, "%s\n", print_r($linkArray, true));
	fclose($f);
	$res = json_encode($linkArray, JSON_FORCE_OBJECT);
	echo $res;
	exit;
}

if ( isset($_REQUEST['listPacks']) )
{
	$packs = getPacks();
	fprintf($f, "%s\n", print_r($packs, true));
	fclose($f);
	echo json_encode($packs);
	exit;
}

$links = json_decode(file_get_contents("php://input"), true);

if ( trim($links['pack']) == "")
	$links['pack'] = 'Default';	
	
$ar = explode(" ", $links['pack']);
$links['pack'] = implode("_", $ar);

fprintf($f, "Pack: %s\n", $links['pack']);
fprintf($f, "%s\n", print_r($links, true));


$path = $basepath."/WebPacks" ;
$query = "";
$i = 0;

if ( count($links['links']) == 0 )
{
	fprintf($f, "Links array is empty");
	echo json_encode($links, JSON_FORCE_OBJECT);
	fclose($f);
	exit;
}
	
foreach($links['links'] as $link=>$value)
{
	if ( $i < count($links) -1 )
		$query.= "tagnroll.com/links/link=\"".$link."\" or "; //should be and ???
	else
		$query.= "tagnroll.com/links/link=\"".$link."\"";
	$i++;	
	
}


addTag($path, $links['pack']);

$path = $path."/".$links['pack'];

$values[$path] = true;

fprintf($f, "%s\n", $query);
fprintf($f, "%s\n", print_r($values, true));

addPack($query, $values);


$linkArray = FluidInfoLink::search($query);

$res = json_encode($linkArray, JSON_FORCE_OBJECT);

fclose($f);

echo $res;

/*
foreach($x->ids as $elem)
{
	
}*/
?>