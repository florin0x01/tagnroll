<?php
if ( isset($_POST['register']) ) 
{
	print_r($_POST);
	if ( empty($_POST['username']) || 
		empty($_POST['password']) || empty($_POST['email']) || empty($_POST['fullname']) )
	{
		print "Fill in all fields ! <br/>";
		exit;
	}
	require_once("../class.backend.factory.php");

	$user = BackendFactory::getType('User', 'FluidInfo');
	$user->setNick($_POST['username']);
	$user->setEmail($_POST['email']);
	$user->setFullName($_POST['fullname']);
	$user->setPassword($_POST['password']);
	$check = $user->Exists();
	
	if ( $check == true )
		print "User exists";
	else {
		print "User does not exist, will create one";
		$user->Register();
	}
}
?>