<?php 
require_once("fluidinfo-php/fluidinfo.php");
require_once("../class.FluidInfoLink.php");
session_start();
header('Content-Type: application/json');

require_once("../class.backend.factory.php");

$response = array('Succes' => '1');
$linkArray = array();

$showHistory="";

//$f = fopen("/tmp/query.txt", "wt");

if (isset($_REQUEST['h'])) 
{
	$showHistory=$_REQUEST['h'];
	//fprintf($f, "%s\n", $showHistory);
	if ( (strcmp($showHistory,"0") == 0) ) $showHistory=$_SESSION['uid'];
}

$tags = json_decode(file_get_contents("php://input"), true);

if ( !count($tags) )
{
	echo json_encode($linkArray, JSON_FORCE_OBJECT);
	exit;
}

//fprintf($f, "%s\n", print_r($tags, true));

$query = "";
if ( $showHistory!="" ) $query = "has tagnroll.com/users/".$showHistory;
else
{
	for($i=0; $i< count($tags[0]); $i++)
	{
		$str = $tags[0][$i];
	
		if ( $i != count($tags[0]) - 1 )
		{
			$query.="has tagnroll.com/tags/".$str." and ";
		}
		else
		{
			$query.="has tagnroll.com/tags/".$str;
		}
	}
}

//fprintf($f, "%s\n", $query);

$oidArray = FluidInfoLink::searchOids($query);

$res = json_encode($oidArray, JSON_FORCE_OBJECT);

//fclose($f);

echo $res;

?>
