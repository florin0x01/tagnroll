<?php 
require_once("fluidinfo-php/fluidinfo.php");
require_once("../class.FluidInfoLink.php");
session_start();
header('Content-Type: application/json');
if (! isset($_SESSION['uid']) )
{
	print "Please login to continue <br />";
	exit;
}

require_once("../class.backend.factory.php");

$response = array('Succes' => '1');

/*
$file_content = file_get_contents("php://input");

if ($file_content == false)
{
	print "This document cannot be tagged. It might be protected.<br />.";
	exit;
}
*/
$inp = json_decode(file_get_contents("php://input"), true);

//$inp = json_decode($file_content, true);

$tags = $inp['tagValues'];
$oid = $inp['id'];

if ( $tags == null || count($tags) == 0)
{
	//print "{}";
	exit;
}

$query = "";

for($i=0; $i< count($tags); $i++)
{
    $str = $tags[$i];

    if ( $i != count($tags) - 1 )
    {
        $query.="has tagnroll.com/tags/".$str." and ";
    }
    else
    {
        $query.="has tagnroll.com/tags/".$str;
    }
}

$query.= " and has tagnroll.com/users/".$_SESSION['uid'];



$link = BackendFactory::getType("Link", "FluidInfo");
$linkArray = FluidInfoLink::searchOids($query, $oid);
error_log("Link array size:".count($linkArray));

//Memory leak ???? Do not uncomment !?
//for($i=0; $i<count($linkArray); $i++) if ( empty($linkArray[$i]) || $linkArray[$i] == 'false' ) $linkArray[$i] = 'google.com';

$res = json_encode($linkArray, JSON_FORCE_OBJECT);

echo $res;
?>
