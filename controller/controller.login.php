<?php
session_start();
if ( isset($_SESSION['uid']) )
	print "Already logged in with user ".$_SESSION['uid']. "<br />";
	
if ( isset($_POST['login']) )
{
	print_r($_POST);
	if ( empty($_POST['username']) || empty($_POST['password']) )
	{
		print "Fill in fields!";
		exit;
	}
	require_once("../class.backend.factory.php");
	
	$user = BackendFactory::getType("User", "FluidInfo");
	$user->setNick($_POST['username']);
	$user->setPassword($_POST['password']);

	$check = $user->Login();
	if ( $check == true ) 
	{
		$_SESSION['uid'] = $user->getNick();
		if (isset($_POST['url']))
			header("Location: http://www.tagnroll.com/search.php?url=".$_POST['url']);
		else 
			print "Login ok";		
	}
	else
	{
		print "Login failed";
	}
}
?>
