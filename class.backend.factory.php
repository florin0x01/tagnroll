<?php
class BackendFactory
{
	/**
	 * 
	 * @return FluidInfoLink
	 */
	public static function getType($role, $backend)
	{
		if ( include_once('class.'.$backend.$role.'.php'))
		{
			$classname = $backend.$role;
			//print "Class: ".$classname;
			return new $classname;
		}
		else
			throw new Exception("Backend ".$backend." does not exist");
	}
}
?>