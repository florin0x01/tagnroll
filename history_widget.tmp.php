<?php
require_once("fluidinfo-php/fluidinfo.php");
require_once("class.FluidInfoLink.php");
session_start();

if ( isset($_GET['uid'])) $uid = $_GET['uid'];
else $uid = $_SESSION['uid'];

$oidArray = FluidInfoLink::listObjects($uid);

print "<div class=\"history-container\" id=\"history-container\"></div>";
?>
<script type="text/javascript">
	$('#history-container').masonry({
		// options
		isFitWidth :true,
		columnWidth: function( containerWidth ) {
			return containerWidth / 300;
		},
		animated:true
	});
</script>
<?php
$global_c = 0;

for ($i=0; $i < count($oidArray); $i++) 
{
	$url = FluidInfoLink::getURL($oidArray[$i]);
	$tags = FluidInfoLink::getTags($oidArray[$i]);
	
	$tagDiv = "<div class=\"history-tags\">";
	$tagCount = 0;
	foreach($tags as $key=>$value) 
	{  
		$tagDiv.='<div class="history-tag"  onclick=tagChange("tag'.$global_c.'") id="tag'.$global_c.'"  >'.$value.'</div>';
		$global_c++;
		
		$tagCount++;
		
		if ($tagCount == 10)
			break;
	}
	$tagDiv.="</div>";
	
	$trunkUrl = substr($url, 0, 22)."...";
	

	$currentTile = '<div class="history-tile" id="history-tile'.$i.'" >'.$tagDiv.'</div>';
	?>
	
	<script type="text/javascript">
		$("#history-container").append('<?php echo $currentTile; ?>');
		showLinkPicture("<?php echo $url; ?>", "<?php echo 'history-tile'.$i; ?>");
	</script>
	
	<?php
	
	
}

?>

	<script type="text/javascript">
	$("#history-container").masonry( 'reload');
	</script>

<?php
?>
